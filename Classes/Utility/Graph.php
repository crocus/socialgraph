<?php
namespace BOROS\Socialgraph\Utility;

use TYPO3\CMS\Core\Utility\DebugUtility as debug;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Marco Neumann <neumann@boros.de>, BOROS GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Graph {

    /**
     *
     * @param string $title
     * @param boolean $setPageTitle
     * @return void
     */
    static public function addTitle($title, $setPageTitle=false) {
        $frontendController = $GLOBALS['TSFE'];
        $frontendController->page['tx_socialgraph_ogtitle_override'] = htmlentities($title);

        if($setPageTitle) {
            $frontendController->page['title'] = $title;
            $frontendController->indexedDocTitle = $title;
        }
    }

    /**
     *
     * @param string $type
     * @return void
     */
    static public function addType($type) {
        $frontendController = $GLOBALS['TSFE'];
        $frontendController->page['tx_socialgraph_ogtype_override'] = $type;
    }

    /**
     *
     * @param string $image
     * @return void
     */
    static public function addImage($image) {
        $frontendController = $GLOBALS['TSFE'];
        $frontendController->page['tx_socialgraph_ogimage_override'] = $image;
    }

    /**
     *
     * @param string $description
     * @param boolean $setPageDescription
     * @return void
     */
    static public function addDescription($description, $setPageDescription=false) {
        $frontendController = $GLOBALS['TSFE'];
        $frontendController->page['tx_socialgraph_ogdescription_override'] = $description;

        if($setPageDescription) {
            $frontendController->page['description'] = $description;
        }
    }

}
