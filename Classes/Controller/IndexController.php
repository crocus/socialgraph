<?php
namespace BOROS\Socialgraph\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Marco Neumann <neumann@boros.de>, BOROS GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\DebugUtility as debug;

/**
* IndexController
*/
class IndexController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
     * pagesRepository
     *
     * @var \BOROS\Socialgraph\Domain\Repository\PagesRepository
     * @inject
     */
    protected $pagesRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     * @inject
     */
    protected $imageService = NULL;

    /**
     * @var array
     */
    protected $confArr = [];

    /**
     * @var array
     */
    protected $pageObj = [];

    /**
     * initializeAction
     *
     * @return void
     */
    public function initializeAction() {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['socialgraph']);

        ArrayUtility::mergeRecursiveWithOverrule($this->confArr, $this->settings, TRUE, FALSE);
        $this->settings = $this->confArr;

        $this->imageService = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Service\\ImageService');

        $currentUid = $GLOBALS['TSFE']->id;
        $this->pageObj = $this->pagesRepository->findByUid($currentUid);
    }

    /**
     * action index
     *
     * @return string
     */
    public function indexAction() {
        // generate title
        if(!empty($GLOBALS['TSFE']->page['tx_socialgraph_ogtitle_override'])) {
            $title = $GLOBALS['TSFE']->page['tx_socialgraph_ogtitle_override'];
        } else {
            if (!empty($this->pageObj->getTxSocialgraphOgtitle())) {
                $title = $this->pageObj->getTxSocialgraphOgtitle();
            } else {
                if (!empty($conf['title'])) {
                    $title = $conf['title'];
                } else {
                    $title = $GLOBALS['TSFE']->page['title'];
                }
            }
        }
        $title = \BOROS\Socialgraph\Utility\Functions::cleanText($title);
        $title = \BOROS\Socialgraph\Utility\Functions::cropText($title, 120);

        // generate type
        if(!empty($GLOBALS['TSFE']->page['tx_socialgraph_ogtype'])) {
            $type = $GLOBALS['TSFE']->page['tx_socialgraph_ogtype'];
        } else {
            if (!empty($this->pageObj->getTxSocialgraphOgtype())) {
                $type = $this->pageObj->getTxSocialgraphOgtype();
            } else {
                if(!empty($conf['type'])) {
                    $type = $conf['type'];
                } else {
                    $type = '';
                }
            }
        }
        $type = htmlentities($type);

        // generate images
        $images = [];
        if(!empty($GLOBALS['TSFE']->page['tx_socialgraph_ogimage_override'])) {
            
            $images = explode(',', $GLOBALS['TSFE']->page['tx_socialgraph_ogimage_override']);
            foreach($images as &$image) {
                $image = $this->renderImage($image, '200c', '200c');
            }
        } else {
            if (!empty($this->pageObj->getTxSocialgraphOgimage())) {
                $images = explode(',', $this->pageObj->getTxSocialgraphOgimage());
                foreach($images as &$image) {
                    $image = $this->renderImage('uploads/tx_socialgraph/' . $image, '200c', '200c');
                }
            } else if($this->settings['defaultImage'] && file_exists(PATH_site . $this->settings['defaultImage'])) {
                $images[0] = $this->renderImage($this->settings['defaultImage'], '200c', '200c');
            } else {
                if($this->settings['appImage']) {
                    $images[0] = $this->renderImage($this->settings['appImage'], '200c', '200c');
                } else {
                    $images[0] = $this->renderImage('typo3conf/ext/socialgraph/Resources/Public/Images/noimage_200.png', '200c', '200c');
                }
            }
        }

        if(!$this->settings['appImage'] || !file_exists(PATH_site . $this->settings['appImage'])) {
            $this->settings['appImage'] = 'typo3conf/ext/socialgraph/Resources/Public/Images/noimage_200.png';
        }

        // generate link
        $link = htmlentities(GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));

        // generate sitename
        if (!empty($conf['sitename'])) {
            $sitename = $conf['sitename'];
        } else {
            $sitename = $GLOBALS['TSFE']->tmpl->setup['sitetitle'];
        }
        $sitename = htmlentities($sitename);

        // generate description
        if(!empty($GLOBALS['TSFE']->page['tx_socialgraph_ogdescription_override'])) {
            $description = $GLOBALS['TSFE']->page['tx_socialgraph_ogdescription_override'];
        } else {
            if (!empty($this->pageObj->getTxSocialgraphOgdescription())) {
                $description = $this->pageObj->getTxSocialgraphOgdescription();
            } else {
                if (!empty($GLOBALS['TSFE']->page['description'])) {
                    $description = $GLOBALS['TSFE']->page['description'];
                } else {
                    $description = $conf['description'];
                }
            }
        }
        $description = \BOROS\Socialgraph\Utility\Functions::cleanText($description);
        $description = \BOROS\Socialgraph\Utility\Functions::cropText($description, 300);
        $description = htmlentities($description);

        // generate locale
        if(is_array($this->settings['locales'])) {
            if($this->settings[locales][$GLOBALS['TSFE']->sys_language_uid]) {
                $locale = $this->settings[locales][$GLOBALS['TSFE']->sys_language_uid];
            }
        }

        // generate canonical
        $urlservice = $this->objectManager->get('BOROS\\Socialgraph\\Service\\UrlService');
        $canonical = $urlservice->getCanonicalUrl($this->settings);

        // generate lastchanged
        $lastchanged = $GLOBALS['TSFE']->page['SYS_LASTCHANGED'] ? $GLOBALS['TSFE']->page['SYS_LASTCHANGED'] : $GLOBALS['TSFE']->page['crdate'];

        $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_socialgraph'] = "\n" . '<!-- This site is optimized with the SocialGraph extension - super cow power for your website -->' . "\n";

        // output SEO
        if($this->settings['activateSEO']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_seoheader'] = '<!-- SEO -->';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_seodate'] = '<meta name="date" content="' . date('Y-m-d', $lastchanged) . '" />';
            if($description) {
                $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_metadescription'] = '<meta name="description" content="' . $description . '">';
            }
            if ($canonical != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_seocanonical'] = '<link rel="canonical" href="' . $canonical . '" />';}
        }

        // Favicon
        if($this->settings['activateFavicon']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_faviconheader'] = '<!-- Favicon -->';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_favicon'] = '<link rel="icon" href="' . $this->renderImage($this->settings['appImage'], '16c', '16c') . '" type="image/png" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_favicon-shortcut-icon'] = '<link rel="shortcut icon" href="' . $this->renderImage($this->settings['appImage'], '16c', '16c') . '" type="image/x-icon" />';
        }

        // output OpenGraph
        if($this->settings['activateOpengraph']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogheader'] = '<!-- Open Graph -->';
            if ($title != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogtitle'] = '<meta property="og:title" content="' . $title . '" />';}
            if ($type != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogtype'] = '<meta property="og:type" content="' . $type . '" />';}
            foreach($images as $k => $image) {
                $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogimage' . $k] = '<meta property="og:image" content="'. $image . '" />';
            }
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogimagewidth'] = '<meta property="og:image:width" content="200" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogimageheight'] = '<meta property="og:image:height" content="200" />';
            if ($link != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogurl'] = '<meta property="og:url" content="' . $link . '" />';}
            if ($sitename != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogsitename'] = '<meta property="og:site_name" content="' . $sitename . '" />';}
            if ($description != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogdescription'] = '<meta property="og:description" content="' . $description . '" />';}
            if ($locale != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_oglocale'] = '<meta property="og:locale" content="' . $locale . '" />';}
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ogupdated_time'] = '<meta property="og:updated_time" content="' . $lastchanged . '" />';
        }

        // output Twitter Cards
        if($this->settings['activateTwittercards']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twitterheader'] = '<!-- Twitter Cards -->';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twittercard'] = '<meta name="twitter:card" content="summary" />';
            if ($title != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twittertitle'] = '<meta name="twitter:title" content="' . $title . '" />';}
            foreach($images as $k => $image) {
                $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twitterimage' . $k] = '<meta name="twitter:image" content="'. $image . '" />';
            }
            if ($description != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twitterdescription'] = '<meta name="twitter:description" content="' . $description . '" />';}
            if ($sitename != '') {$GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_twitterdomain'] = '<meta name="twitter:domain" content="' . $sitename . '" />';}
        }

        // output app icons
        if($this->settings['activateAppimages'] && $this->settings['appImage']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_app-iconheader'] = '<!-- App images -->';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ms-tile-image'] = '<meta name="msapplication-TileImage" content="' . $this->renderImage($this->settings['appImage'], '144c', '144c') . '" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_ms-tile-color'] = '<meta name="msapplication-TileColor" content="#ffffff" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_apple-touch-icon'] = '<link href="' . $this->renderImage($this->settings['appImage'], '200c', '200c') . '" rel="apple-touch-icon" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_apple-touch-icon-76x76'] = '<link href="' . $this->renderImage($this->settings['appImage'], '76c', '76c') . '" rel="apple-touch-icon" sizes="76x76" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_apple-touch-icon-120x120'] = '<link href="' . $this->renderImage($this->settings['appImage'], '120c', '120c') . '" rel="apple-touch-icon" sizes="120x120" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_apple-touch-icon-152x152'] = '<link href="' . $this->renderImage($this->settings['appImage'], '152c', '152c') . '" rel="apple-touch-icon" sizes="152x152" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_apple-touch-icon-180x180'] = '<link href="' . $this->renderImage($this->settings['appImage'], '180c', '180c') . '" rel="apple-touch-icon" sizes="180x180" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_icon-hires'] = '<link href="' . $this->renderImage($this->settings['appImage'], '192c', '192c') . '" rel="icon" sizes="192x192" />';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_icon-normal'] = '<link href="' . $this->renderImage($this->settings['appImage'], '128c', '128c') . '" rel="icon" sizes="128x128" />';
        }

        // output hreflang
        if($this->settings['activateHreflang']) {
            $metaHreflangBuilder = $this->objectManager->get('BOROS\\Socialgraph\\Utility\\MetaHreflangBuilder');
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_hreflangheader'] = '<!-- hreflang -->';
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_hreflang'] = $metaHreflangBuilder->getHreflang($this->settings);
        }

        // Output schema.org json data
        if($this->settings['fbProfileUrl'] || $this->settings['instagramProfileUrl']  || $this->settings['gplusProfileUrl']  || $this->settings['linkedinProfileUrl']) {
            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_app-jsonldheader'] = '<!-- JSON-LD schema.org -->';

            $profileArr = [];
            if($this->settings['fbProfileUrl']) $profileArr['fbProfileUrl'] = $this->settings['fbProfileUrl'];
            if($this->settings['instagramProfileUrl']) $profileArr['instagramProfileUrl'] = $this->settings['instagramProfileUrl'];
            if($this->settings['gplusProfileUrl']) $profileArr['gplusProfileUrl'] = $this->settings['gplusProfileUrl'];
            if($this->settings['linkedinProfileUrl']) $profileArr['linkedinProfileUrl'] = $this->settings['linkedinProfileUrl'];

            $addressArr = [];
            $addressArr['@type'] = 'PostalAddress';
            if($this->settings['streetAddress']) $addressArr['streetAddress'] = $this->settings['streetAddress'];
            if($this->settings['addressLocality']) $addressArr['addressLocality'] = $this->settings['addressLocality'];
            if($this->settings['addressRegion']) $addressArr['addressRegion'] = $this->settings['addressRegion'];
            if($this->settings['postalCode']) $addressArr['postalCode'] = $this->settings['postalCode'];
            if($this->settings['telephone']) $addressArr['telephone'] = $this->settings['telephone'];

            $jsonld = (object)array(
                "@context" => "http://schema.org",
                "@type" => "Organization",
                "name" => $sitename,
                "url" => $link,
                "logo" => $this->renderImage($this->settings['appImage'], '200c', '200c'),
                "address" => $addressArr,
                "sameAs" => array_values($profileArr)
            );

            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_app-jsonldcontent'] = '<script type="application/ld+json">' . "\n" . json_encode($jsonld, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) . "\n" . '</script>';

            $jsonld = (object)array(
                "@context" => "http://schema.org",
                "@type" => "WebSite",
                "name" => $sitename,
                "url" => GeneralUtility::getIndpEnv('TYPO3_SITE_URL')
            );

            $GLOBALS['TSFE']->additionalHeaderData[$this->extensionName . '_app-jsonldcontent2'] = '<script type="application/ld+json">' . "\n" . json_encode($jsonld, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) . "\n" . '</script>';
        }

        return false;
    }

    /**
     * action share
     *
     * @return string
     */
    public function widgetAction() {
        $this->view->assign('pageObj', $this->pageObj);
        $this->view->assign('settings', $this->settings);
    }

    /**
     * Resizes a given image (if required) and renders the respective img tag
     *
     * @see http://typo3.org/documentation/document-library/references/doc_core_tsref/4.2.0/view/1/5/#id4164427
     * @param string $src a path to a file, a combined FAL identifier or an uid (integer). If $treatIdAsReference is set, the integer is considered the uid of the sys_file_reference record. If you already got a FAL object, consider using the $image parameter instead
     * @param string $width width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
     * @param string $height height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.
     * @param integer $minWidth minimum width of the image
     * @param integer $minHeight minimum height of the image
     * @param integer $maxWidth maximum width of the image
     * @param integer $maxHeight maximum height of the image
     * @param boolean $treatIdAsReference given src argument is a sys_file_reference record
     * @param FileInterface|AbstractFileFolder $image a FAL object
     * @return string Rendered tag
     * @throws \Exception
     */
    protected function renderImage($src = NULL, $width = NULL, $height = NULL, $minWidth = NULL, $minHeight = NULL, $maxWidth = NULL, $maxHeight = NULL, $treatIdAsReference = FALSE, $image = NULL) {
        if (is_null($src) && is_null($image) || !is_null($src) && !is_null($image)) {
            throw new \Exception('You must either specify a string src or a File object.', 1382284106);
        }

        if($src) {
            $treatIdAsReference = is_numeric($src) ? true : false;
        }
        $image = $this->imageService->getImage($src, $image, true);

        $processingInstructions = [
            'width' => $width,
            'height' => $height,
            'minWidth' => $minWidth,
            'minHeight' => $minHeight,
            'maxWidth' => $maxWidth,
            'maxHeight' => $maxHeight,
            'treatIdAsReference' => $treatIdAsReference
        ];

        $processedImage = $this->imageService->applyProcessingInstructions($image, $processingInstructions);
        return $this->imageService->getImageUri($processedImage, true);
    }

}
