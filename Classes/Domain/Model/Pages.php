<?php
namespace BOROS\Socialgraph\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Marco Neumann <neumann@boros.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class for the pages
 *
 * represents the Page Model
 *
 *
 */

class Pages extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * uid
     * @var integer
     */
    protected $uid;

    /**
     * pid
     * @var integer
     */
    protected $pid;

    /**
     * sorting
     * @var int
     */
    protected $sorting;

    /**
     * title
     * @var string
     *
     */
    protected $title;

    /**
     * subtitle
     * @var string
     *
     */
    protected $subtitle;

    /**
     * tx_socialgraph_ogtitle
     * @var string
     *
     */
    protected $txSocialgraphOgtitle;

    /**
     * tx_socialgraph_ogtype
     * @var string
     *
     */
    protected $txSocialgraphOgtype;

    /**
     * tx_socialgraph_ogimage
     * @var string
     *
     */
    protected $txSocialgraphOgimage;

    /**
     * tx_socialgraph_ogdescription
     * @var string
     *
     */
    protected $txSocialgraphOgdescription;

    /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Returns the pid
     *
     * @return int $pid
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Returns the sorting
     *
     * @return int $sorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @return string $txSocialgraphOgtitle
     */
    public function getTxSocialgraphOgtitle()
    {
        return $this->txSocialgraphOgtitle;
    }

    /**
     * @return string
     */
    public function getTxSocialgraphOgtype()
    {
        return $this->txSocialgraphOgtype;
    }

    /**
     * @return string
     */
    public function getTxSocialgraphOgimage()
    {
        return $this->txSocialgraphOgimage;
    }

    /**
     * @return string
     */
    public function getTxSocialgraphOgdescription()
    {
        return $this->txSocialgraphOgdescription;
    }
}