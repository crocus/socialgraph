<?php
namespace BOROS\Socialgraph\ViewHelpers\Share;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Marco Neumann <neumann@boros.de>, BOROS GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * ViewHelper to render facebook link
 *
 * # Example: Basic Example
 * # Description: Render the content of the VH as page title
 * <code>
 *	<sg:share.email subject="The subject to render" body="The body to render" />
 * </code>
 *
 */

class TumblrViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * Arguments initialization
     *
     * @return void
     */
    public function initializeArguments() {
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('name', 'string', 'Specifies the name of an anchor');
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document');
        $this->registerTagAttribute('rev', 'string', 'Specifies the relationship between the linked document and the current document');
        $this->registerTagAttribute('target', 'string', 'Specifies where to open the linked document');
    }

    /**
     * @param string $linkurl
     * @return string Rendered facebook link
     */
    public function render($linkurl = '') {
        $linkHref = 'https://www.tumblr.com/share/'.rawurlencode($linkurl);
        $tagContent = $this->renderChildren();
        if ($tagContent !== NULL) {
            $linkText = $tagContent;
        }
        $this->tag->setContent($linkText);

        $escapeSpecialCharacters = !isset($GLOBALS['TSFE']->spamProtectEmailAddresses) || $GLOBALS['TSFE']->spamProtectEmailAddresses !== 'ascii';
        $this->tag->addAttribute('href', $linkHref, $escapeSpecialCharacters);
        $this->tag->addAttribute('target', '_blank');
        $this->tag->forceClosingTag(TRUE);
        return $this->tag->render();
    }
}
