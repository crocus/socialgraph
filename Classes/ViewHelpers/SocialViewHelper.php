<?php
namespace BOROS\Socialgraph\ViewHelpers;

use TYPO3\CMS\Core\Utility\DebugUtility as debug;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Marco Neumann <neumann@boros.de>, BOROS GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * ViewHelper to render the page title
 *
 * # Example: Basic Example
 * # Description: Render the content of the VH as page title
 * <code>
 *	<sg:social title="The title to render" description="The description to render" setPageTitle="true" setPageDescription="true" />
 * </code>
 *
 */

class SocialViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Override the title tag
	 *
	 * @param string $title
	 * @param string $description
	 * @param string $image
	 * @param boolean $setPageTitle
	 * @param boolean $setPageDescription
	 *
	 * @return void
	 */
	public function render($title='', $description='', $image='', $setPageTitle=false, $setPageDescription=false) {
		if (!empty($title)) {
			\BOROS\Socialgraph\Utility\Graph::addTitle($title, $setPageTitle);
		}

		if (!empty($description)) {
			\BOROS\Socialgraph\Utility\Graph::addDescription($description, $setPageDescription);
		}

		if (!empty($image)) {
			\BOROS\Socialgraph\Utility\Graph::addImage($image);
		}
	}
}
