<?php
if (!defined ('TYPO3_MODE')) {
     die ('Access denied.');
}

$TYPO3_CONF_VARS['FE']['pageOverlayFields'] .= ',tx_socialgraph_ogtitle,tx_socialgraph_ogtype,tx_socialgraph_ogimage,tx_socialgraph_ogdescription';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'BOROS.' . $_EXTKEY,
    'Frontend',
    array(
        'Index' => 'index'
    ),
    array(
    )
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'BOROS.' . $_EXTKEY,
    'Widget',
    array(
        'Index' => 'widget'
    ),
    array(
    ),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
mod.wizards.newContentElement.wizardItems.common.header = Inhaltselemente

mod.wizards.newContentElement.wizardItems.common.elements.socialgraph_share {
  title = Share Icons
  description = Adds Share-Icons to the page
  icon = ../typo3conf/ext/socialgraph/Resources/Public/Icons/share_24.gif
  tt_content_defValues {
    CType = socialgraph_share
  }
}
mod.wizards.newContentElement.wizardItems.common.show := addToList(socialgraph_share)

');

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][] =
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('socialgraph') . '/Classes/Hooks/ContentPostProcessor.php:BOROS\\Socialgraph\\Hooks\\ContentPostProcessor->render_Cache';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] =
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('socialgraph') . '/Classes/Hooks/ContentPostProcessor.php:BOROS\\Socialgraph\\Hooks\\ContentPostProcessor->render_noCache';

//$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Hooks/CmsLayout.php:BOROS\Socialgraph\Hooks\CmsLayout';

?>