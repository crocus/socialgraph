#
# Table structure for table 'pages'
#
CREATE TABLE pages (
	tx_socialgraph_ogtitle tinytext,
	tx_socialgraph_ogtype tinytext,
	tx_socialgraph_ogimage text,
	tx_socialgraph_ogdescription text,
	tx_socialgraph_canonical_url varchar(255) NOT NULL DEFAULT '',
	tx_socialgraph_noindex tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tx_socialgraph_nofollow tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tx_socialgraph_notranslate tinyint(4) unsigned DEFAULT '0' NOT NULL
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
	tx_socialgraph_ogtitle tinytext,
	tx_socialgraph_ogtype tinytext,
	tx_socialgraph_ogimage text,
	tx_socialgraph_ogdescription tinytext,
	tx_socialgraph_canonical_url varchar(255) NOT NULL DEFAULT '',
	tx_socialgraph_noindex tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tx_socialgraph_nofollow tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tx_socialgraph_notranslate tinyint(4) unsigned DEFAULT '0' NOT NULL
);