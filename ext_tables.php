<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Social Graph');
$TCA['tt_content']['columns']['pi_flexform']['config']['ds_pointerField'] = 'list_type,CType';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($_EXTKEY,'Widget','Share Icons', 'EXT:socialgraph/Resources/Public/Icons/share_16.gif');
$TCA['tt_content']['columns']['pi_flexform']['config']['ds'][',socialgraph_widget'] = 'FILE:EXT:'.$_EXTKEY.'/Configuration/FlexForms/share.xml';
$TCA['tt_content']['types']['socialgraph_widget']['showitem'] = 'CType,hidden,pi_flexform';

// Create array with columns
$tempColumns = array (
	'tx_socialgraph_ogtitle' => array (
		'exclude' => 1,
		'label' => 'Title (og:title, twitter:title)',
		'config' => array (
			'type' => 'input',
			'size' => '60',
		)
	),
	'tx_socialgraph_ogtype' => array (
		'exclude' => 1,
		'label' => 'Type (og:type)',
		'config' => array (
			'type' => 'input',
			'size' => '30',
		)
	),
	'tx_socialgraph_ogimage' => array (
		'exclude' => 1,
		'label' => 'Image (og:image, twitter:image)',
		'config' => array (
			'type' => 'group',
			'internal_type' => 'file',
			'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
			'max_size' => $GLOBALS['TYPO3_CONF_VARS']['BE']['maxFileSize'],
			'uploadfolder' => 'uploads/tx_socialgraph',
			'show_thumbs' => 1,
			'size' => 4,
			'minitems' => 0,
			'maxitems' => 6,
		)
	),
	'tx_socialgraph_ogdescription' => array (
		'exclude' => 1,
		'label' => 'Description (og:description, twitter:description)',
		'config' => array(
			'type' => 'text',
			'cols' => '30',
			'rows' => '5'
		)
	),
    'tx_socialgraph_canonical_url' => Array(
        'exclude' => 1,
        'label' => 'Canonical URL',
        'config' => array(
            'type' => 'input',
            'size' => '50',
            'max' => '255',
            'checkbox' => '',
            'eval' => 'trim',
            'placeholder' => 'http://www.example.com/directory/page.html',
            'wizards' => Array(
                '_PADDING' => 2,
                'link' => Array(
                    'type' => 'popup',
                    'title' => 'Link',
                    'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                    'module' => array(
                        'name' => 'wizard_element_browser',
                        'urlParameters' => array(
                            'mode' => 'wizard',
                            'act' => 'url'
                        )
                    ),
                    'params' => array(
                        'blindLinkOptions' => 'mail',
                    ),
                    'JSopenParams' => 'width=800,height=600,status=0,menubar=0,scrollbars=1'
                ),
            ),
        ),
    ),
    'tx_socialgraph_noindex' => Array(
        'exclude' => 1,
        'label' => 'No index',
        'config' => array(
            'type' => 'check',
            'default' => 0
        )
    ),
    'tx_socialgraph_nofollow' => Array(
        'exclude' => 1,
        'label' => 'No follow',
        'config' => array(
            'type' => 'check',
            'default' => 0
        )
    ),
    'tx_socialgraph_notranslate' => Array(
        'exclude' => 1,
        'label' => 'No translate',
        'config' => array(
            'type' => 'check',
            'default' => 0
        )
    ),
);

$GLOBALS['TYPO3_CONF_VARS']['FE']['pageOverlayFields'] .= ',tx_socialgraph_ogtitle,tx_socialgraph_ogtype,tx_socialgraph_ogimage,tx_socialgraph_ogdescription,tx_socialgraph_canonical_url,tx_socialgraph_noindex,tx_socialgraph_nofollow,tx_socialgraph_notranslate,';

// Add columns to TCA of pages and pages_language_overlay
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tempColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages','--div--;Social Graph / SEO,tx_socialgraph_ogtitle;;;;1-1-1, tx_socialgraph_ogtype, tx_socialgraph_ogimage, tx_socialgraph_ogdescription, tx_socialgraph_canonical_url, tx_socialgraph_noindex, tx_socialgraph_nofollow, tx_socialgraph_notranslate');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay',$tempColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages_language_overlay','--div--;Social Graph / SEO,tx_socialgraph_ogtitle;;;;1-1-1, tx_socialgraph_ogtype, tx_socialgraph_ogimage, tx_socialgraph_ogdescription, tx_socialgraph_canonical_url, tx_socialgraph_noindex, tx_socialgraph_nofollow, tx_socialgraph_notranslate');

