plugin.tx_socialgraph {
	view {
		# cat=plugin.tx_seldb/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:socialgraph/Resources/Private/Templates/
		# cat=plugin.tx_seldb/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:socialgraph/Resources/Private/Partials/
		# cat=plugin.tx_seldb/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:socialgraph/Resources/Private/Layouts/
	}
	persistence {
		storagePid =
	}
}
