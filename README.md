# ext-socialgraph
Social Graph - Adds the Open Graph, Twitter Cards and App images properties in meta-tags to the html-header and supports multilanguage-websites.

Fully based on Extbase and only working on TYPO3 > 6.2
